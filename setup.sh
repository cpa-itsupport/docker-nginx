#!/bin/sh

if [ -f /site.zip ];
then
  echo "site.zip was downloaded. Ignore" 
else
  if [ -n "$SITE_ZIP_URL" ]; then
    echo "downloading thte size zip file from URL $SITE_ZIP_URL"
    wget -O /site.zip $SITE_ZIP_URL
    rm -Rf /usr/share/nginx/html/*
    sed -i '/index  index\.html/i try_files\ $uri\ \/index.html;' /etc/nginx/conf.d/default.conf
    unzip /site.zip -d /usr/share/nginx/html/
  else
    echo "variable SITE_ZIP_URL is not set."
  fi
fi
nginx -g 'daemon off;'
